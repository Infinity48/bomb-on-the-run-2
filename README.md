# Bomb On The Run 2

I am not sure what this is going to be yet.
https://gamejolt.com/games/Bomb_on_the_run_2/569612

## Building

Things needed:
>Python 3.6 or later  
Pyinstaller  
Arcade  
GamejoltAPI  
The source code for the game

1. Download the sourcecode for the game.
2. Make a new file called 'apikeys.py' and put this (without the hashtag line) into it:
```
# Public API key file, trophies will not work with this!
GAME_ID = ''
PRIVATE_KEY = ''
```
3. If Python is in your PATH (or the linux equivalent) then you can use the build scripts in the repo unmodified. If you don't, add `python -m` or `python3 -m` to the start of the command.
