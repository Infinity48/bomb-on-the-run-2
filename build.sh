!#/bin/sh
pyinstaller __init__.py --onefile -w
mv dist/__init__ ./botr2.appimage
chmod +x ./botr2.appimage
rm -rf __pycache__
rm -rf build
rm -rf dist