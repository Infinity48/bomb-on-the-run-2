# Bomb On The Run 2 - No API Version

This is a version of BOTR2 with the gamejolt API stuff removed. There is no `__init__.py` as it would be identical to the normal version's.

## Building

Things needed:
>Python 3.6 or later  
Pyinstaller  
Arcade  
The source code for the game  
These files

1. Download the normal version of the source code and this version.
2. Copy the py files from this folder over the normal versions.
3. Use the normal build scripts.
