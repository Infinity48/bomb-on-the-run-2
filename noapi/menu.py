''' Bomb On The Run 2
	Copyright (C) 2021 Infinity48

	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at https://mozilla.org/MPL/2.0/.
'''
from main import *
import game

curkey = 0

class Menu(arcade.View):
	''' Class that manages the 'menu' view. '''
	class PlayButton(arcade.gui.UIFlatButton):
		def on_click(self):
			game.continuing = False
			gamev = game.Game()
			gamev.setup()
			window.show_view(gamev)
	
	class InfoButton(arcade.gui.UIFlatButton):
		def on_click(self):
			info = InfoScreen()
			info.setup()
			window.show_view(info)

	class ContinueButton(arcade.gui.UIFlatButton):
		def on_click(self):
			game.continuing = True
			gamev = game.Game()
			gamev.setup()
			window.show_view(gamev)
			
	class QuitButton(arcade.gui.UIFlatButton):
		def on_click(self):
			arcade.close_window()
	
	class SettingsButton(arcade.gui.UIFlatButton):
		def on_click(self):
			option = OptionsScreen()
			option.setup()
			window.show_view(option)

	def __init__(self):
		super().__init__()
		self.ui_manager = arcade.gui.UIManager()
		self.music = None
		arcade.gui.ui_style.UIStyle.default_style().set_class_attrs(None, font_name='fonts/DejaVuSans', font_size=20)

	def on_show(self):
		''' Called when switching to this view'''
		self.setup()
		arcade.set_background_color(arcade.color.WHITE)

	def on_hide_view(self):
		self.ui_manager.unregister_handlers()
		arcade.stop_sound(self.current_player)

	def on_draw(self):
		''' Draw the menu '''
		arcade.start_render()
		arcade.draw_lrwh_rectangle_textured(SCREEN_WIDTH/8, SCREEN_HEIGHT/2,
											472, 113,
											arcade.load_texture('images/logo.png'))
		arcade.draw_text(VERSION, 32, 16,
						arcade.color.BLACK, font_size=12, anchor_x='center', font_name='fonts/DejaVuSans')

	def setup(self):
		''' Set up this view. '''
		self.ui_manager.purge_ui_elements()
		if os.path.exists('../save.json'):
			button = self.ContinueButton(
				'Continue',
				center_x=320,
				center_y=200,
				width=250,
			)
			self.ui_manager.add_ui_element(button)
		button = self.PlayButton(
			'New Game',
			center_x=320,
			center_y=160,
			width=250,
		)
		self.ui_manager.add_ui_element(button)
		button = self.SettingsButton(
			'Settings',
			center_x=320,
			center_y=120,
			width=250,
		)
		self.ui_manager.add_ui_element(button)
		button = self.InfoButton(
			'Info',
			center_x=320,
			center_y=80,
			width=250,
		)
		self.ui_manager.add_ui_element(button)
		button = self.QuitButton(
			'Quit',
			center_x=320,
			center_y=40,
			width=250,
		)
		self.ui_manager.add_ui_element(button)
		if not self.music:
			self.music = arcade.Sound(':resources:music/1918.mp3', streaming=True) # Title screen music, currently placeholder.
			self.current_player = self.music.play(SETTINGS['MUSIC_VOLUME'], 0, True)

class InfoScreen(arcade.View):
	''' Info screen, kind of self explanatey. '''
	class BackButton(arcade.gui.UIFlatButton):
		def on_click(self):
			menu = Menu()
			menu.setup()
			window.show_view(menu)
	
	def __init__(self):
		super().__init__()
		self.ui_manager = arcade.gui.UIManager()

	def on_show(self):
		''' Called when switching to this view'''
		self.setup()
		arcade.set_background_color(arcade.color.WHITE)

	def on_hide_view(self):
		self.ui_manager.unregister_handlers()

	def on_draw(self):
		''' Draw the menu '''
		arcade.start_render()
		arcade.draw_text(self.infotext, SCREEN_WIDTH/2, SCREEN_HEIGHT/2,
						arcade.color.BLACK, font_size=12, anchor_x='center', anchor_y='center', align='center', font_name='fonts/DejaVuSansCondensed')

	def setup(self):
		''' Set up this view. '''
		self.ui_manager.purge_ui_elements()
		button = self.BackButton(
			'Back',
			center_x=320,
			center_y=80,
			width=250
		)
		self.ui_manager.add_ui_element(button)
		with open('info.txt') as f:
			self.infotext = f.read()

class OptionsScreen(arcade.View):
	class BackButton(arcade.gui.UIFlatButton):
		def on_click(self):
			with open('../settings.json', 'w') as f:
				json.dump(SETTINGS, f)
			menu = Menu()
			menu.setup()
			window.show_view(menu)
	class MusicButtons():
		class PlusMusic(arcade.gui.UIFlatButton):
			def on_click(self):
				if SETTINGS['MUSIC_VOLUME'] < 1.0:
					SETTINGS['MUSIC_VOLUME'] += 0.1
		class MinusMusic(arcade.gui.UIFlatButton):
			def on_click(self):
				if SETTINGS['MUSIC_VOLUME'] > 0.1:
					SETTINGS['MUSIC_VOLUME'] -= 0.1
		class PlusSFX(arcade.gui.UIFlatButton):
			def on_click(self):
				if SETTINGS['SFX_VOLUME'] < 1.0:
					SETTINGS['SFX_VOLUME'] += 0.1
		class MinusSFX(arcade.gui.UIFlatButton):
			def on_click(self):
				if SETTINGS['SFX_VOLUME'] > 0.1:
					SETTINGS['SFX_VOLUME'] -= 0.1
	class ControlButtons():
		class SetUp(arcade.gui.UIFlatButton):
			def on_click(self):
				global curkey
				SETTINGS['KEY_UP'] = curkey
		class SetDown(arcade.gui.UIFlatButton):
			def on_click(self):
				global curkey
				SETTINGS['KEY_DOWN'] = curkey
		class SetLeft(arcade.gui.UIFlatButton):
			def on_click(self):
				global curkey
				SETTINGS['KEY_LEFT'] = curkey
		class SetRight(arcade.gui.UIFlatButton):
			def on_click(self):
				global curkey
				SETTINGS['KEY_RIGHT'] = curkey
	class ApiThings():
		UsernameBox = arcade.gui.UIInputBox(width=300, center_x=160, center_y=80, text=SETTINGS['USERNAME'])
		TokenBox = arcade.gui.UIInputBox(width=300, center_x=SCREEN_WIDTH-160, center_y=80, text=SETTINGS['TOKEN'])
	
	def __init__(self):
		super().__init__()
		self.ui_manager = arcade.gui.UIManager()
	
	def on_show(self):
		self.setup()
		arcade.set_background_color(arcade.color.WHITE)
		
	def on_hide_view(self):
		self.ui_manager.unregister_handlers()
	
	def on_draw(self):
		''' Draw the menu '''
		arcade.start_render()
		os.chdir('fonts')
		arcade.draw_text(f"Current Music Volume: {round(SETTINGS['MUSIC_VOLUME'], 1)}", SCREEN_WIDTH/2, SCREEN_HEIGHT-60,
						arcade.color.BLACK, font_size=12, anchor_x='center', font_name='DejaVuSans')
		arcade.draw_text(f"Current SFX Volume: {round(SETTINGS['SFX_VOLUME'], 1)}", SCREEN_WIDTH/2, SCREEN_HEIGHT-120,
						arcade.color.BLACK, font_size=12, anchor_x='center', font_name='DejaVuSans')
		arcade.draw_text(f"Current Up Key: {SETTINGS['KEY_UP']}", SCREEN_WIDTH/2, SCREEN_HEIGHT-200,
						arcade.color.BLACK, font_size=12, anchor_x='center', font_name='DejaVuSans')
		arcade.draw_text(f"Current Down Key: {SETTINGS['KEY_DOWN']}", SCREEN_WIDTH/2, SCREEN_HEIGHT-250,
						arcade.color.BLACK, font_size=12, anchor_x='center', font_name='DejaVuSans')
		arcade.draw_text(f"Current Left Key: {SETTINGS['KEY_LEFT']}", SCREEN_WIDTH/2, SCREEN_HEIGHT-300,
						arcade.color.BLACK, font_size=12, anchor_x='center', font_name='DejaVuSans')
		arcade.draw_text(f"Current Right Key: {SETTINGS['KEY_RIGHT']}", SCREEN_WIDTH/2, SCREEN_HEIGHT-350,
						arcade.color.BLACK, font_size=12, anchor_x='center', font_name='DejaVuSans')
		os.chdir('..')
	
	def on_key_press(self, key, modifiers):
		global curkey
		curkey = key
	
	def setup(self):
		self.ui_manager.purge_ui_elements()
		button = self.BackButton(
			'Back',
			center_x=320,
			center_y=40,
			width=250
		)
		self.ui_manager.add_ui_element(button)
		button = self.MusicButtons.PlusMusic(
			'+',
			center_x=SCREEN_WIDTH-100,
			center_y=SCREEN_HEIGHT-50,
			width=50
		)
		self.ui_manager.add_ui_element(button)
		button = self.MusicButtons.MinusMusic(
			'-',
			center_x=100,
			center_y=SCREEN_HEIGHT-50,
			width=50
		)
		self.ui_manager.add_ui_element(button)
		button = self.MusicButtons.PlusSFX(
			'+',
			center_x=SCREEN_WIDTH-100,
			center_y=SCREEN_HEIGHT-110,
			width=50
		)
		self.ui_manager.add_ui_element(button)
		button = self.MusicButtons.MinusSFX(
			'-',
			center_x=100,
			center_y=SCREEN_HEIGHT-110,
			width=50
		)
		self.ui_manager.add_ui_element(button)
		button = self.ControlButtons.SetUp(
			'SET',
			center_x=SCREEN_WIDTH-100,
			center_y=SCREEN_HEIGHT-180,
			width=100
		)
		self.ui_manager.add_ui_element(button)
		button = self.ControlButtons.SetDown(
			'SET',
			center_x=SCREEN_WIDTH-100,
			center_y=SCREEN_HEIGHT-230,
			width=100
		)
		self.ui_manager.add_ui_element(button)
		button = self.ControlButtons.SetLeft(
			'SET',
			center_x=SCREEN_WIDTH-100,
			center_y=SCREEN_HEIGHT-280,
			width=100
		)
		self.ui_manager.add_ui_element(button)
		button = self.ControlButtons.SetRight(
			'SET',
			center_x=SCREEN_WIDTH-100,
			center_y=SCREEN_HEIGHT-330,
			width=100
		)
		self.ui_manager.add_ui_element(button)