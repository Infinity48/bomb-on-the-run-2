''' Bomb On The Run 2
	Copyright (C) 2021 Infinity48

	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at https://mozilla.org/MPL/2.0/.
'''
import arcade, arcade.gui, os, random, math, json, gamejoltapi, apikeys

SPRITE_SCALING = 1
SPRITE_NATIVE_SIZE = 32
SPRITE_SIZE = int(SPRITE_NATIVE_SIZE * SPRITE_SCALING)

SCREEN_WIDTH = SPRITE_SIZE * 20
SCREEN_HEIGHT = SPRITE_SIZE * 15
SCREEN_TITLE = 'Bomb On The Run 2'
VERSION = '0.4.1'

MOVEMENT_SPEED = SPRITE_SIZE / 4

window = arcade.Window(SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_TITLE)

if os.path.exists('settings.json'):
	with open('settings.json', 'r') as f:
		SETTINGS = json.load(f)
else:
	SETTINGS = {'MUSIC_VOLUME':1.0,'SFX_VOLUME':1.0,'KEY_UP':arcade.key.UP,'KEY_DOWN':arcade.key.DOWN,'KEY_LEFT':arcade.key.LEFT,'KEY_RIGHT':arcade.key.RIGHT,'USERNAME':'','TOKEN':''}
	with open('settings.json', 'w') as f:
		json.dump(SETTINGS, f)