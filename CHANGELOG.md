# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.4.1] - 2021-07-10
### Changed
- The boss now has invincibility time between hits.
- Some minor animation bugs.
- The credits now scroll slower.

## [0.4.0] - 2021-06-27
### Added
- A boss battle.
- Dialogue portaits and sound effects.
- End credits.
- GameJolt Trophies.
### Changed
- The save format so more data can be stored.
- Updated the guide to modding.

## [0.3.1] - 2021-06-12
### Added
- A basic guide to modding.
### Changed
- The game now has a set font so the font used no longer changes between platforms.
- Removed pointless if statment on the room loading logic and restored the try and except pair from previous versions.
### Fixed
- Dialogue lines with more than one newline no longer go off the dialogue box.

## [0.3.0] - 2021-06-07
### Added
- Dialogue boxes for story.
- An extra room.
### Changed
- Some room designs.
- The folder structure has been changed for better organisation.

## [0.2.1] - 2021-06-05
### Added
- A settings screen that gives you:
    - The ability to remap controls.
    - Volume control.
- More rooms.

## [0.2.0] - 2021-05-31
### Added
- More rooms.
### Changed
- Rooms file data is no longer hardcoded, making custom rooms easier.
### Fixed
- Prisoners no longer go though walls when released.

## [0.1.5] - 2021-05-30
### Added
- Background music.
- More rooms.
### Changed
- Made where the prisoners go after release no longer be hardcoded.
### Removed
- Got rid of 'play_song()' function in 'Game' due to only being called in 'setup()'.

## [0.1.0] - 2021-05-29
### Added
- Saving.
### Changed
- The game files are no longer embedded in the executable.
