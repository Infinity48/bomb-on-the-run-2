pyinstaller __init__.py --onefile -w -i icon.ico
copy dist\__init__.exe botr2.exe
del dist /Q
rd dist
del build\__init__ /Q
rd build\__init__
rd build
del __pycache__ /Q
rd __pycache__