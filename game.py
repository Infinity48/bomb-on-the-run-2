''' Bomb On The Run 2
	Copyright (C) 2021 Infinity48

	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at https://mozilla.org/MPL/2.0/.
'''
from main import *

continuing = False
api = gamejoltapi.GameJoltAPI(apikeys.GAME_ID, apikeys.PRIVATE_KEY, username=SETTINGS['USERNAME'], userToken=SETTINGS['TOKEN'], submitRequests=True)

class Game(arcade.View):
	''' Main application class. '''
	
	class DialogueBox():
		def __init__(self):
			self.dialogue = ['This message sould not appear. If it does, tell me.', 'wait.....bomb on the run TWO?!']
			self.show = False
			self.line = 0
			self.portrait = arcade.Sprite(None, SPRITE_SCALING)
			self.portrait.textures = arcade.load_spritesheet('images/portraits.png', 128, 128, 4, 4, 0)
			self.sounds = [arcade.Sound('sounds/bombsay.ogg'),arcade.Sound('sounds/bosssay.ogg'),arcade.Sound('sounds/henchsay.ogg')]
		
		def set_portrait(self):
			# Character portrait code. There's probably a better way of doing this.
				if self.dialogue[self.line].startswith('bomb:'):
					self.dialogue[self.line] = self.dialogue[self.line].replace('bomb:','')
					self.portrait.set_texture(0)
					self.portrait.center_x = SCREEN_WIDTH-128
					self.portrait.center_y = 176
					self.sounds[0].play(SETTINGS['SFX_VOLUME'])
				elif self.dialogue[self.line].startswith('boss:'):
					self.dialogue[self.line] = self.dialogue[self.line].replace('boss:','')
					self.portrait.set_texture(1)
					self.portrait.center_x = 128
					self.portrait.center_y = 176
					self.sounds[1].play(SETTINGS['SFX_VOLUME'])
				elif self.dialogue[self.line].startswith('boss-angry:'):
					self.dialogue[self.line] = self.dialogue[self.line].replace('boss-angry:','')
					self.portrait.set_texture(2)
					self.portrait.center_x = 128
					self.portrait.center_y = 176
					self.sounds[1].play(SETTINGS['SFX_VOLUME'])
				elif self.dialogue[self.line].startswith('henchman:'):
					self.dialogue[self.line] = self.dialogue[self.line].replace('henchman:','')
					self.portrait.set_texture(3)
					self.portrait.center_x = 128
					self.portrait.center_y = 176
					self.sounds[2].play(SETTINGS['SFX_VOLUME'])
		
		def load(self, x, y):
			self.line = 0
			if os.path.exists(f'dialogue/{x}_{y}.txt'):
				with open(f'dialogue/{x}_{y}.txt', 'r') as f:
					self.dialogue = json.load(f)
				self.set_portrait()
				self.show = True
		
		def draw(self):
			if self.show:
				self.portrait.draw()
				arcade.draw_rectangle_filled(SCREEN_WIDTH/2, 64, 600, 100, arcade.color.BLACK)
				arcade.draw_text(self.dialogue[self.line], 20, 110, arcade.color.WHITE, font_name='fonts/DejaVuSans', font_size=10, anchor_y='top')
				arcade.draw_text('Space for next', SCREEN_WIDTH-100, 20, arcade.color.WHITE, 8, font_name='fonts/DejaVuSans-Oblique')
		
		def next(self):
			if self.line != len(self.dialogue) - 1:
				self.line += 1
				self.set_portrait()
			else:
				self.show = False
	
	class Explosion(arcade.AnimatedTimeSprite):
		def __init__(self):
			super().__init__()
			self.start_time = 0
			self.textures = arcade.load_spritesheet('images/explosion.png', 32, 32, 5, 5, 0)
			self.texture_change_frames = 15
			self.scale = 1.5
			
		def update(self, curtime):
			if curtime >= self.start_time + 1/3:
				self.center_x = SCREEN_WIDTH * 2
			self.update_animation()
		
	class FinalBoss(arcade.AnimatedTimeSprite):
		def __init__(self):
			super().__init__()
			self.textures = arcade.load_spritesheet('images/boss.png', 48, 64, 3, 3, 0)
			self.texture = self.textures[0]
			self.texture_change_frames = 12
			self.change_x = 0
			self.change_y = 0
			self.hp = 5
			self.start_time = 0
			self.stunned = False
		
		def update(self, target_x, target_y, curtime):
			''' This is same 'AI' from the enemies. '''
			global api
			if self.stunned:
				self.change_x = 0
				self.change_y = 0
				if curtime >= self.start_time + 3:
					self.stunned = False
			elif self.hp < 1:
				self.kill()
				api.trophiesAddAchieved(143829)
				credits = Credits()
				window.show_view(credits)
			else:
				if random.randrange(150) == 0:
					start_x = self.center_x
					start_y = self.center_y

					# Do some maths to calculate how to get the boss to the destination.
					x_diff = target_x - start_x
					y_diff = target_y - start_y
					angle = math.atan2(y_diff, x_diff)

					# Taking into account the angle, calculate our change_x and change_y.
					self.change_x = math.cos(angle) * MOVEMENT_SPEED / 5
					self.change_y = math.sin(angle) * MOVEMENT_SPEED / 5
				self.update_animation()
		
		def draw(self):
			super().draw()
			arcade.draw_rectangle_filled(self.center_x, self.center_y - 40, 10 * self.hp, 10, arcade.color.GREEN)

	def __init__(self):
		'''
		Initializer
		'''
		super().__init__()
		# Controls which room you are in.
		self.roomx = 0
		self.roomy = 0
		
		self.crashed = False
		self.spawn = 0
		self.player = arcade.Sprite('images/bomb.png', SPRITE_SCALING)
		self.timer = 0
		global continuing
		global api
		
		print(continuing)
		
		# Keeps track of progress and loads savefile if it exists.
		if continuing:
			with open('../save.json', 'r') as f:
				self.save_data = json.load(f)
		else:
			self.save_data = {'levers_pulled':0,'rooms':[[{'dialogue':False,'lever':False},{'dialogue':False,'lever':False},{'dialogue':False,'lever':False},{'dialogue':False,'lever':False},{'dialogue':False,'lever':False},{'dialogue':False,'lever':False}],
								[{'dialogue':False,'lever':False},{'dialogue':False,'lever':False},{'dialogue':False,'lever':False},{'dialogue':False,'lever':False},{'dialogue':False,'lever':False},{'dialogue':False,'lever':False}],
								[{'dialogue':False,'lever':False},{'dialogue':False,'lever':False},{'dialogue':False,'lever':False},{'dialogue':False,'lever':False},{'dialogue':False,'lever':False},{'dialogue':False,'lever':False}],
								[{'dialogue':False,'lever':False},{'dialogue':False,'lever':False},{'dialogue':False,'lever':False},{'dialogue':False,'lever':False},{'dialogue':False,'lever':False},{'dialogue':False,'lever':False}],
								[{'dialogue':False,'lever':False},{'dialogue':False,'lever':False},{'dialogue':False,'lever':False},{'dialogue':False,'lever':False},{'dialogue':False,'lever':False},{'dialogue':False,'lever':False}],
								[{'dialogue':False,'lever':False},{'dialogue':False,'lever':False},{'dialogue':False,'lever':False},{'dialogue':False,'lever':False},{'dialogue':False,'lever':False},{'dialogue':False,'lever':False}]]}
		
		# Data for room filenames and what exit leads to what room.
		with open('rooms.json', 'r') as f:
				self.room_data = json.load(f)
		# Load sounds.
		try:
			os.chdir('sounds')
			self.lever_sound = arcade.Sound('lever.ogg')
			self.death_sounds = [arcade.Sound('death1.ogg'),arcade.Sound('death2.ogg')]
			self.music = arcade.Sound('enigmatic-adventure.ogg', streaming=True)
			self.music.playing = False
			self.explosion_sound = arcade.Sound('small-explosion.ogg')
			os.chdir('..')
		except Exception as err:
			print('Error loading sounds!')
			print(err)
			self.crashed = True
		self.dialogue = self.DialogueBox()
		
		# Trophy Data, checks time from internet instead of datetime to stop cheating.
		try:
			api.trophiesAddAchieved(143059)
			if api.time()['day'] == '27' and api.time()['month'] == '11':
				api.trophiesAddAchieved(143060)
		except:
			pass
		self.explosion = self.Explosion()
		self.explosion.center_x = SCREEN_WIDTH * 2
		

	def setup(self):
		''' Set up the game and initialize the variables. '''
		# Set up the player
		if self.spawn == 1:
			self.player.center_x = 40
			self.player.center_y = SCREEN_HEIGHT / 2
		elif self.spawn == 2:
			self.player.center_x = SCREEN_WIDTH - 40
			self.player.center_y = SCREEN_HEIGHT / 2
		elif self.spawn == 3:
			self.player.center_x = SCREEN_WIDTH / 2
			self.player.center_y = 40
		elif self.spawn == 4:
			self.player.center_x = SCREEN_WIDTH / 2
			self.player.center_y = SCREEN_HEIGHT - 40
		else:
			self.player.center_x = 100
			self.player.center_y = 100

		# Read in the tiled map
		try:
			os.chdir('rooms')
			map = arcade.tilemap.read_tmx(self.room_data['rooms'][self.roomy][self.roomx])
			os.chdir('..')

			# Layer processing.
			self.wall_list = arcade.tilemap.process_layer(map_object=map,
														layer_name='Walls',
														scaling=SPRITE_SCALING,
														use_spatial_hash=True)
			self.lever_list = arcade.tilemap.process_layer(map_object=map,
														layer_name='Levers',
														scaling=SPRITE_SCALING,
														use_spatial_hash=True)
			self.gate_list = arcade.tilemap.process_layer(map_object=map,
														layer_name='Gates',
														scaling=SPRITE_SCALING,
														use_spatial_hash=True)
			self.bg = arcade.tilemap.process_layer(map_object=map,
														layer_name='BG',
														scaling=SPRITE_SCALING,
														use_spatial_hash=True)
			self.enemy_list = arcade.tilemap.process_layer(map_object=map,
														layer_name='Enemies',
														scaling=SPRITE_SCALING)
			self.prisoner_list = arcade.tilemap.process_layer(map_object=map,
														layer_name='Prisoners',
														scaling=SPRITE_SCALING)
			self.prisdest = list(arcade.tilemap.process_layer(map_object=map,
														layer_name='prisdest'))
			self.bossspawn = list(arcade.tilemap.process_layer(map_object=map,
														layer_name='bossspawn'))
			self.bossgate = arcade.tilemap.process_layer(map_object=map,
														layer_name='BossGate',
														scaling=SPRITE_SCALING,
														use_spatial_hash=True)
			if self.save_data['levers_pulled'] < self.room_data['levers']:
				self.wall_list.extend(self.bossgate)
			# Other stuff
			if self.roomx == self.room_data['bossroom'][0] and self.roomy == self.room_data['bossroom'][1]:
				self.boss = self.FinalBoss()
				self.enemy_list.append(self.boss)
				self.boss.center_x = self.bossspawn[0].center_x
				self.boss.center_y = self.bossspawn[0].center_y
			else:
				self.boss = None
			# Set the background color
			if map.background_color:
				arcade.set_background_color(map.background_color)
			# Create a physics engine for this room
			self.player.physics_engine = arcade.PhysicsEngineSimple(self.player, self.wall_list)
			for enemy in self.enemy_list:
				enemy.physics_engine = arcade.PhysicsEngineSimple(enemy, self.wall_list)
			for prisoner in self.prisoner_list:
				prisoner.physics_engine = arcade.PhysicsEngineSimple(prisoner, self.wall_list)
			
			# Makes sure the levers keep their on sprite after room refresh.
			if self.save_data['rooms'][self.roomy][self.roomx]['lever']:
				for lever in self.lever_list:
					lever.append_texture(arcade.load_texture("images/lever_on.png"))
					lever.set_texture(1)
				del self.prisoner_list
			else:
				self.wall_list.extend(self.gate_list)
			if not self.save_data['rooms'][self.roomy][self.roomx]['dialogue']:
				self.dialogue.load(self.roomx, self.roomy)
				self.save_data['rooms'][self.roomy][self.roomx]['dialogue'] = True
		except Exception as err:
			print('Error loading map file!')
			print(err)
			self.crashed = True
		
		# Save data to file.
		with open('../save.json', 'w') as f:
			json.dump(self.save_data, f)
		
		''' Play the Background music. '''
		# Check if the music is already playing.
		if not self.music.playing:
			self.music_player = self.music.play(SETTINGS['MUSIC_VOLUME'], 0, True)
			self.music.playing = True
		
		#print(self.roomx, self.roomy, self.save_data['rooms'][self.roomy][self.roomx], len(self.wall_list), len(self.lever_list), len(self.gate_list), len(self.enemy_list), len(self.prisoner_list))

	def on_draw(self):
		'''
		Render the screen.
		'''
		# This command has to happen before we start drawing
		arcade.start_render()
		
		if self.crashed:
			arcade.draw_text('Oops, the game broke! If this happened in the base game, please report it. If it happened in a mod, tell the developer(s)!', SCREEN_WIDTH/2, SCREEN_HEIGHT/2,
							arcade.color.BLACK, font_size=10, anchor_x='center')
		else:
			# Draw all the tiles in this room and player sprite.
			self.bg.draw()
			self.lever_list.draw()
			try:
				self.prisoner_list.draw()
			except AttributeError:
				pass
			self.enemy_list.draw()
			self.player.draw()
			self.wall_list.draw()
			if self.boss:
				self.boss.draw()
			try:
				self.explosion.draw()
			except Exception:
				pass
			self.dialogue.draw()


	def on_key_press(self, key, modifiers):
		'''Called whenever a key is pressed. '''

		if not self.dialogue.show:
			if key == SETTINGS['KEY_UP']:
				self.player.change_y = MOVEMENT_SPEED
			elif key == SETTINGS['KEY_DOWN']:
				self.player.change_y = -MOVEMENT_SPEED
			elif key == SETTINGS['KEY_LEFT']:
				self.player.change_x = -MOVEMENT_SPEED
			elif key == SETTINGS['KEY_RIGHT']:
				self.player.change_x = MOVEMENT_SPEED

	def on_key_release(self, key, modifiers):
		'''Called when the user releases a key. '''

		if key == SETTINGS['KEY_UP'] or key == SETTINGS['KEY_DOWN']:
			self.player.change_y = 0
		elif key == SETTINGS['KEY_LEFT'] or key == SETTINGS['KEY_RIGHT']:
			self.player.change_x = 0
		elif key == arcade.key.SPACE:
			self.dialogue.next()
		'''elif key == arcade.key.J:
			self.save_data['levers_pulled'] += 1
			print(self.save_data['levers_pulled'])
		elif key == arcade.key.K:
			credits = Credits()
			window.show_view(credits)'''

	def on_update(self, delta_time):
		''' Movement and game logic '''
		# Call update on all sprites.
		self.player.physics_engine.update()
		self.explosion.update(self.timer)
		for enemy in self.enemy_list:
			enemy.physics_engine.update()
		
		self.timer += delta_time

		# Do some logic here to figure out what room we are in, and if we need to go to a different room. Also, colision detection. 
		# Object colision logic.
		if arcade.check_for_collision_with_list(self.player, self.lever_list):
			if not self.save_data['rooms'][self.roomy][self.roomx]['lever']:
				self.save_data['rooms'][self.roomy][self.roomx]['lever'] = True
				arcade.play_sound(self.lever_sound, SETTINGS['SFX_VOLUME'])
				self.save_data['levers_pulled'] += 1
			for gate in self.gate_list:
				gate.kill()
			if self.save_data['levers_pulled'] >= self.room_data['levers']:
				for gate in self.bossgate:
					gate.kill()
		
		if self.save_data['rooms'][self.roomy][self.roomx]['lever'] and len(self.gate_list) == 0:
			for lever in self.lever_list:
				lever.append_texture(arcade.load_texture("images/lever_on.png"))
				lever.set_texture(1)
			try:
				for prisoner in self.prisoner_list:
					if prisoner.center_y > self.prisdest[0].center_y:
						if prisoner.center_y != self.prisdest[0].center_y:
							prisoner.center_y -= MOVEMENT_SPEED
					elif prisoner.center_y < self.prisdest[0].center_y:
						if prisoner.center_y != self.prisdest[0].center_y:
							prisoner.center_y += MOVEMENT_SPEED
					if prisoner.center_x > self.prisdest[0].center_x:
						if prisoner.center_x != self.prisdest[0].center_x:
							prisoner.center_x -= MOVEMENT_SPEED
					elif prisoner.center_x < self.prisdest[0].center_x:
						if prisoner.center_x != self.prisdest[0].center_x:
							prisoner.center_x += MOVEMENT_SPEED
					if prisoner.center_x < self.prisdest[0].center_x and prisoner.center_y > self.prisdest[0].center_y:
						prisoner.kill()
					prisoner.physics_engine.update()
			except AttributeError:
				pass
		
		if arcade.check_for_collision_with_list(self.player, self.enemy_list):
			if self.save_data['rooms'][self.roomy][self.roomx]['lever']:
				self.save_data['rooms'][self.roomy][self.roomx]['lever'] = False
				self.save_data['levers_pulled'] -= 1
			arcade.play_sound(self.death_sounds[random.randint(0,1)], SETTINGS['SFX_VOLUME'])
			self.setup()

		# Room exit logic.
		if self.player.center_x > SCREEN_WIDTH:
			self.roomx += 1
			self.spawn = 1
			self.setup()
		elif self.player.center_x < 0:
			self.roomx -= 1
			self.spawn = 2
			self.setup()
		elif self.player.center_y > SCREEN_HEIGHT:
			self.roomy -= 1
			self.spawn = 3
			self.setup()
		elif self.player.center_y < 0:
			self.roomy += 1
			self.spawn = 4
			self.setup()

		if not self.dialogue.show:
			for enemy in self.enemy_list:
				enemy.center_x += enemy.change_x
				enemy.center_y += enemy.change_y
				''' Random 1 in 75 chance that we'll change from our old direction and
				then re-aim toward the player. '''
				if random.randrange(75) == 0:
					enemy.start_x = enemy.center_x
					enemy.start_y = enemy.center_y

					# Get the destination location for the enemy
					enemy.dest_x = self.player.center_x
					enemy.dest_y = self.player.center_y

					# Do some maths to calculate how to get the enemy to the destination.
					enemy.x_diff = enemy.dest_x - enemy.start_x
					enemy.y_diff = enemy.dest_y - enemy.start_y
					angle = math.atan2(enemy.y_diff, enemy.x_diff)

					# Taking into account the angle, calculate our change_x and change_y.
					enemy.change_x = math.cos(angle) * MOVEMENT_SPEED / 4
					enemy.change_y = math.sin(angle) * MOVEMENT_SPEED / 4
			if self.boss:
				'''Boss battle code'''
				self.boss.update(self.player.center_x, self.player.center_y, self.timer)
				if arcade.check_for_collision_with_list(self.boss, self.enemy_list) and not self.boss.stunned:
					self.boss.stunned = True
					self.boss.hp -= 1
					self.boss.start_time = self.timer # Needed to figure out how long is left on stun timer.
					for enemy in self.enemy_list:
						if arcade.check_for_collision(enemy, self.boss) and enemy != self.boss:
							enemy.kill()
					self.explosion = self.Explosion()
					self.explosion.center_x = self.boss.center_x
					self.explosion.center_y = self.boss.center_y
					self.explosion.start_time = self.timer
					arcade.play_sound(self.explosion_sound, SETTINGS['SFX_VOLUME'])
		else:
			self.player.change_x = 0
			self.player.change_y = 0
	
	def on_hide_view(self):
		arcade.stop_sound(self.music_player)

class Credits(arcade.View):
	def __init__(self):
		super().__init__()
		self.music = arcade.Sound(':resources:music/funkyrobot.mp3', streaming=True)
		self.scroll = 0
		with open('credits.txt', 'r') as f:
			self.text = f.read()
	
	def on_draw(self):
		arcade.start_render()
		arcade.draw_text(self.text, SCREEN_WIDTH/2, self.scroll,
						arcade.color.WHITE, font_size=10.5, anchor_x='center', anchor_y ='top', align='center', font_name='fonts/DejaVuSansCondensed')
		if self.scroll > 2500:
			arcade.draw_text('You may now close the game.', SCREEN_WIDTH/2, SCREEN_HEIGHT/16,
						arcade.color.ORANGE, font_size=12, anchor_x='center', anchor_y ='top', align='center', font_name='fonts/DejaVuSans-Bold')
	
	def on_update(self, delta_time):
		if self.scroll <= 2500:
			self.scroll += 0.5
	
	def on_show(self):
		self.music.play(SETTINGS['MUSIC_VOLUME'], 0, True)
		arcade.set_background_color(arcade.color.BLACK)