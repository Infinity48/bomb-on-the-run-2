<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.5" tiledversion="1.6.0" name="Factory" tilewidth="32" tileheight="32" tilecount="8" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="0">
  <image width="32" height="32" source="images/wall.png"/>
  <objectgroup draworder="index" id="2">
   <object id="2" x="0" y="0" width="32" height="32"/>
  </objectgroup>
 </tile>
 <tile id="2">
  <image width="32" height="32" source="images/lever_off.png"/>
  <objectgroup draworder="index" id="2">
   <object id="1" x="1.875" y="30.75">
    <polygon points="0.249513,1.49708 6.62451,-6.77519 9.875,-6.75 20.125,-21.875 20.125,-25 27,-24.875 27.125,-17 20.125,-16.875 13.125,-6.875 22.1226,-6.97529 28.375,1.09932"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="4">
  <image width="32" height="32" source="images/gatelr.png"/>
  <objectgroup draworder="index" id="3">
   <object id="6" x="0" y="11" width="32" height="7"/>
  </objectgroup>
 </tile>
 <tile id="5">
  <image width="32" height="32" source="images/gateud.png"/>
  <objectgroup draworder="index" id="2">
   <object id="3" x="14" y="0" width="7" height="32.0278"/>
  </objectgroup>
 </tile>
 <tile id="6">
  <image width="32" height="32" source="images/bomb.png"/>
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="32" height="32"/>
   <object id="3" x="0" y="1" width="32" height="31"/>
   <object id="4" x="0" y="1" width="32" height="31"/>
  </objectgroup>
 </tile>
 <tile id="7">
  <image width="32" height="32" source="images/dynamite.png"/>
  <objectgroup draworder="index" id="2">
   <object id="2" x="4" y="1" width="24" height="29"/>
  </objectgroup>
 </tile>
 <tile id="8">
  <image width="32" height="32" source="images/enemy.png"/>
  <objectgroup draworder="index" id="2">
   <object id="16" x="0" y="1" width="32" height="31"/>
  </objectgroup>
 </tile>
 <tile id="9">
  <image width="32" height="32" source="images/box.png"/>
  <objectgroup draworder="index" id="2">
   <object id="3" x="0" y="0" width="32" height="32"/>
  </objectgroup>
 </tile>
</tileset>
