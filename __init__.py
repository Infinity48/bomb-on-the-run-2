''' Bomb On The Run 2
	Copyright (C) 2021 Infinity48

	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at https://mozilla.org/MPL/2.0/.
'''
''' TODO:
	Dehardcode the text strings for different languge capability.
	Add a pause screen.
'''
from main import *
from menu import Menu

# Set the working directory.
os.chdir('assets')

def main():
	''' Main method '''
	window.show_view(Menu())
	arcade.run()

if __name__ == '__main__':
	main()